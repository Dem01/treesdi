import { injectable } from "inversify";

interface ITree {
  name: string;
  gallery: string;
  position: string;
  _id?: string;
}

@injectable()
export class Tree implements ITree {
  constructor(
    public name: string,
    public gallery: string,
    public position: string,
    public _id?: string
  ) {}
}
