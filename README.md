# Trees DI

Trees DI is a web application based on Dependency Injection pattern for trees database management.

Tested on npm 7.14.0 & node v15.4.0

## Installation & Run

Use the package manager [npm](https://www.npmjs.com/) to install Trees DI.

```bash
npm i
npm run start
```

Make sure to check /db/connection.ts file for mongodb connection configuration

## Usage

```bash
Open web browser
Type localhost:4000 to run the app locally
```

![image](./assets/screen.png)

## Technology used

- node.js & express as a web server
- [inversify](https://inversify.io/) as inversion of control container
- [ejs](https://ejs.co/) as view engine
- typescript
- mongodb

## API

```
HTTP GET /api/trees

[
    {
        "_id": "60b5471cdb92b7422893015d",
        "name": "drzewo",
        "position": "41 24.2028, 2 10.4418",
        "gallery": "/gallery/etui.jpg"
    }
]

HTTP POST /api/trees

    {
        "name": string,
        "position": string,
        "gallery": string
    }
```
