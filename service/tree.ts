import { inject, injectable } from "inversify";
import { Tree } from "../model/tree";
import TYPES from "../types/types";
import { DB } from "../db/client";

@injectable()
export class TreeService {
  private db: DB;

  constructor(@inject(TYPES.DB) db: DB) {
    this.db = db;
  }

  public getTrees(): Promise<Tree[]> {
    return new Promise<Tree[]>((res, rej) => {
      this.db.find("tree", {}, (err, data: Tree[]) => res(data));
    });
  }

  public newTree(tree: Tree): Promise<Tree> {
    return new Promise<Tree>((res, rej) => {
      this.db.insert("tree", tree, (error, data: Tree) => {
        res(data);
      });
    });
  }
}
