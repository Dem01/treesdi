import "reflect-metadata";
import * as bodyParser from "body-parser";
import { InversifyExpressServer } from "inversify-express-utils";
import { Container } from "inversify";
import { DB } from "./db/client";
import TYPES from "./types/types";
import { TreeService } from "./service/tree";
import "./controller/tree";
import fetch from "node-fetch";
import * as fileUpload from "express-fileupload";
import * as path from "path";
import * as express from "express";

const container = new Container();

// IoC binding
container.bind<DB>(TYPES.DB).to(DB);
container.bind<TreeService>(TYPES.TreeService).to(TreeService);

let server = new InversifyExpressServer(container);
server.setConfig((app) => {
  app.use(
    bodyParser.urlencoded({
      extended: true,
    })
  );
  app.use(bodyParser.json());
  app.use(fileUpload());
  app.use("/gallery", express.static("gallery"));

  app.set("view engine", "ejs");

  //views
  app.get("/", (req, res) => {
    fetch("http://localhost:4000/api/tree")
      .then((response) => response.json())
      .then((data) => {
        res.render("index", { data });
      });
  });

  app.post("/", (req, res) => {
    if (!req.files) {
      return res.status(400).send("No files uploaded");
    }

    let f = req.files.file as fileUpload.UploadedFile;

    f.mv(path.join(__dirname, "gallery", f.name), (err) => {
      if (err) return res.status(500).send(err);

      req.body.gallery = `/gallery/${f.name}`;

      console.log(JSON.stringify(req.body));

      fetch("http://localhost:4000/api/tree", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(req.body),
      })
        .then((res) => res.json())
        .then(() => {
          res.send("OK");
        });
    });
  });
});

let app = server.build();
app.listen(4000);
console.log("Server started on port 4000");
