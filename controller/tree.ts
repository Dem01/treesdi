import { controller, httpGet, httpPost } from "inversify-express-utils";
import { inject } from "inversify";
import { Tree } from "../model/tree";
import { TreeService } from "../service/tree";
import TYPES from "../types/types";
import { Request } from "express";

@controller("/api/tree")
export class TreeController {
  constructor(@inject(TYPES.TreeService) private treeService: TreeService) {}

  @httpGet("/")
  public getTrees(): Promise<Tree[]> {
    return this.treeService.getTrees();
  }

  @httpPost("/")
  public newTree(request: Request): Promise<Tree> {
    return this.treeService.newTree(request.body);
  }
}
