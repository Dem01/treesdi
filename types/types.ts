const TYPES = {
  TreeService: Symbol.for("TreeService"),
  DB: Symbol.for("DB"),
};

export default TYPES;
