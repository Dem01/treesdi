import { injectable } from "inversify";
import { Db } from "mongodb";
import { MongoDB } from "./connection";
import { Tree } from "../model/tree";

@injectable()
export class DB {
  public db: Db;

  constructor() {
    MongoDB.getConnection((connection) => {
      this.db = connection;
    });
  }

  public find(
    collection: string,
    filter: Object,
    result: (error, data) => void
  ): void {
    this.db
      .collection(collection)
      .find(filter)
      .toArray((error, find) => {
        return result(error, find);
      });
  }

  public insert(
    collection: string,
    model: Tree,
    result: (error, data) => void
  ): void {
    this.db.collection(collection).insertOne(model, (error, insert) => {
      return result(error, insert.ops[0]);
    });
  }
}
